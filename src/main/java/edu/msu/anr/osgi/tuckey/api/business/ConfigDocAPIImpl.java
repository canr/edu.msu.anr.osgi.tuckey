package edu.msu.anr.osgi.tuckey.api.business;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.dotcms.repackage.org.apache.commons.collections.MapUtils;
import com.dotcms.repackage.org.tuckey.web.filters.urlrewrite.Rule;
import com.dotcms.repackage.org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import com.dotmarketing.business.DotStateException;
import com.dotmarketing.common.db.DotConnect;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.filters.DotUrlRewriteFilter;
import com.dotmarketing.util.Logger;
import com.dotmarketing.util.UUIDGenerator;
import com.liferay.portal.model.User;
import com.liferay.util.FileUtil;

import edu.msu.anr.osgi.tuckey.api.model.ConfigDoc;
import edu.msu.anr.osgi.tuckey.api.model.ConfigDocValidationException;

/**
 * Implementation of {@link ConfigDocAPI}.
 */
public class ConfigDocAPIImpl implements ConfigDocAPI {

    @Override
    public List<ConfigDoc> list() throws DotDataException {
        String query = "SELECT * FROM tuckeyconfigdocs";
        return castResults((new DotConnect()).setSQL(query).loadObjectResults());
    }

    @Override
    public ConfigDoc save(String xml, User user) throws Exception {
        return this.save(xml, "", user);
    }

    @Override
    public ConfigDoc save(String xml, String title, User user) throws Exception {
        ConfigDoc newDoc = new ConfigDoc(xml, UUIDGenerator.generateUuid(),
            false, false, title, user.getUserId(), new Date());
        if (!newDoc.isValid()) {
            throw new ConfigDocValidationException("Unable to save invalid ConfigDoc.", newDoc.getErrors());
        }
        return this.save(newDoc);
    }

    @Override
    synchronized public ConfigDoc save(ConfigDoc doc) throws Exception {
        String query = "INSERT INTO tuckeyconfigdocs (inode, contents, archived, live, title, modUser, modDate) "+
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        (new DotConnect()).setSQL(query)
                .addParam(doc.getInode())
                .addParam(doc.getContents())
                .addParam(doc.isArchived())
                .addParam(doc.isLive())
                .addParam(doc.getTitle())
                .addParam(doc.getModUser())
                .addParam(doc.getModDate())
                .loadResult();

        return doc;
    }

    @Override
    public ConfigDoc get(String inode) throws DotDataException {
        String query = "SELECT * FROM tuckeyconfigdocs WHERE inode=?";
        return castResults((new DotConnect()).setSQL(query, 1)
                .addParam(inode)
                .loadObjectResults()).get(0);
    }

    @Override
    public ConfigDoc getLive() throws DotDataException {
        String query = "SELECT * FROM tuckeyconfigdocs WHERE live=true";
        return castResults((new DotConnect().setSQL(query, 1)
            .loadObjectResults())).get(0);
    }

    @Override
    public ConfigDoc load(String inode) throws Exception {
        return this.load(this.get(inode));
    }

    @Override
    synchronized public ConfigDoc load(ConfigDoc doc) throws DotDataException {
        try {
            // Clear rules
            for (List<Rule> existingRules = DotUrlRewriteFilter.getUrlRewriteFilter().getRules();
                 !existingRules.isEmpty();
                 existingRules = DotUrlRewriteFilter.getUrlRewriteFilter().getRules()) {
                Rule currentExistingRule = existingRules.get(0);
                DotUrlRewriteFilter.getUrlRewriteFilter().removeRule(currentExistingRule);
            }

            // Load ruleset
            for (Object ruleObj : doc.getConf().getRules()) {
                Rule rule = (Rule) ruleObj;
                DotUrlRewriteFilter.getUrlRewriteFilter().addRule(rule);
            }

            // Mark as live
            return _setLive(doc);
        } catch (Exception e) {
            Logger.error(this, "Failed to load ConfigDoc "+doc.getInode()+". "+e.getMessage());
            load(getLive());
            return doc;
        }
    }

    @Override
    public ConfigDoc archive(String inode) throws DotStateException, DotDataException, DotSecurityException {
        return this.archive(this.get(inode));
    }

    @Override
    public ConfigDoc archive(ConfigDoc doc) throws DotStateException, DotDataException, DotSecurityException {
        return _setArchived(doc, true);
    }

    @Override
    public ConfigDoc unarchive(String inode) throws DotDataException, DotSecurityException {
        return this.unarchive(this.get(inode));
    }

    @Override
    public ConfigDoc unarchive(ConfigDoc doc) throws DotDataException, DotSecurityException {
        return _setArchived(doc, false);
    }

    @Override
    public void delete(String inode) throws DotDataException, DotStateException, DotSecurityException {
        delete(this.get(inode));
    }

    @Override
    public void delete(ConfigDoc doc) throws DotDataException, DotStateException, DotSecurityException {
        if (doc.isLive()) {
            throw new DotStateException("Cannot delete the live ConfigDoc.");
        }

        String query = "DELETE FROM tuckeyconfigdocs WHERE inode=?";
        (new DotConnect()).setSQL(query)
                .addParam(doc.getInode())
                .loadResult();
    }

    @Override
    public ConfigDoc readConfigFile() throws IOException {
        Path urlrewritePath = Paths.get(FileUtil.getRealPath(UrlRewriteFilter.DEFAULT_WEB_CONF_PATH));
        byte[] encodedConfig = Files.readAllBytes(urlrewritePath);
        String configXml = new String(encodedConfig, StandardCharsets.UTF_8);

        return new ConfigDoc(configXml);
    }

    /**
     * Casts a list of query result maps to a list of {@link ConfigDoc}s.
     * @param queryResults List of result maps returned by {@link DotConnect#loadResults()}.
     * @return List of result {@link ConfigDoc}s.
     */
    private List<ConfigDoc> castResults(List<Map<String, Object>> queryResults) {
        List<ConfigDoc> docs = new ArrayList<>();
        if (queryResults != null) {
            for (Map<String, Object> result : queryResults) {
                ConfigDoc doc = castResult(result);
                docs.add(doc);
            }
        }
        return docs;
    }

    /**
     * Casts a query result map to a {@link ConfigDoc}.
     * @param queryResult A result map returned by {@link DotConnect#loadResults()}.
     * @return Result map's corresponding {@link ConfigDoc} or null if the result is null.
     */
    private ConfigDoc castResult(Map<String, Object> queryResult) {
        ConfigDoc doc = null;
        if (queryResult != null) {
            // Parse date
            String dateStr = MapUtils.getString(queryResult, "moddate", "");
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            Date date;
            try {
                date = fmt.parse(dateStr);
            } catch (ParseException e) {
                date = new Date(0);
            }

            // Load data
            doc = new ConfigDoc(
                MapUtils.getString(queryResult, "contents", ""),
                MapUtils.getString(queryResult, "inode", ""),
                MapUtils.getBooleanValue(queryResult, "archived", false),
                MapUtils.getBooleanValue(queryResult, "live", false),
                MapUtils.getString(queryResult, "title", ""),
                MapUtils.getString(queryResult, "moduser", ""),
                date
            );
        }

        return doc;
    }

    /**
     * Sets the state of the "archived" property in the given object and the database.
     * @param doc The {@link ConfigDoc} to be archived or unarchived.
     * @param archived The new value of the "archived" property.
     * @return The modified {@link ConfigDoc}.
     * @throws DotStateException If the {@link ConfigDoc} is live.
     * @throws DotDataException If the {@link ConfigDoc} can't be updated in the database.
     */
    synchronized private ConfigDoc _setArchived(ConfigDoc doc, boolean archived) throws DotStateException, DotDataException, DotSecurityException {
        if (archived && doc.isLive()) {
            throw new DotStateException("Cannot archive the live ConfigDoc.");
        }
        String query = "UPDATE tuckeyconfigdocs SET archived=? WHERE inode=?";
        (new DotConnect()).setSQL(query)
                .addParam(archived)
                .addParam(doc.getInode())
                .loadResult();
        doc.setArchived(archived);

        return doc;
    }

    /**
     * Marks a given doc as live.
     * @param doc The {@link ConfigDoc} to be made live.
     * @return The modified {@link ConfigDoc}.
     * @throws DotDataException If the {@link ConfigDoc} can't be updated in the database.
     */
    synchronized private ConfigDoc _setLive(ConfigDoc doc) throws DotDataException {
        // Make this doc live
        String query = "UPDATE tuckeyconfigdocs SET live=true WHERE inode=?";
        (new DotConnect()).setSQL(query)
                .addParam(doc.getInode())
                .loadResult();

        // Make all other docs not live
        try {
            query = "UPDATE tuckeyconfigdocs SET live=false WHERE NOT inode=?";
            (new DotConnect()).setSQL(query)
                    .addParam(doc.getInode())
                    .loadResult();
        } catch (DotDataException e) {
            // Revert making this doc live
            query = "UPDATE tuckeyconfigdocs SET live=false WHERE inode=?";
            (new DotConnect()).setSQL(query)
                    .addParam(doc.getInode())
                    .loadResult();
        }

        // Update doc object
        doc.setLive(true);

        return doc;
    }

}
