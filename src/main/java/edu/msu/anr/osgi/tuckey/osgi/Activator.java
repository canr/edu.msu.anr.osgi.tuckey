package edu.msu.anr.osgi.tuckey.osgi;

import com.dotcms.rest.config.RestServiceUtil;
import com.dotmarketing.common.db.DotConnect;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.osgi.GenericBundleActivator;
import org.osgi.framework.BundleContext;

import edu.msu.anr.osgi.tuckey.api.rest.ConfigDocResource;

public class Activator extends GenericBundleActivator {

    private final String[] xmls = new String[]{"conf/portlet.xml", "conf/liferay-portlet.xml"};

    @SuppressWarnings ("unchecked")
    public void start ( BundleContext context ) throws Exception {

        initializeServices ( context );

        createConfigDocTable();

        RestServiceUtil.addResource(ConfigDocResource.class);

        registerPortlets(context, xmls);
    }

    public void stop ( BundleContext context ) throws Exception {

        RestServiceUtil.removeResource(ConfigDocResource.class);

        //Unregister all the bundle services
        unregisterServices( context );
    }

    private void createConfigDocTable() throws DotDataException {
        String query = "CREATE TABLE IF NOT EXISTS tuckeyconfigdocs ("+
                    "inode VARCHAR(36) NOT NULL, "+
                    "contents TEXT NOT NULL, "+
                    "archived BOOL NOT NULL, "+
                    "live BOOL NOT NULL, "+
                    "title VARCHAR(255), "+
                    "modUser VARCHAR(255), "+
                    "modDate TIMESTAMP NOT NULL," +
                    "PRIMARY KEY (inode)"+
                ")";
        (new DotConnect()).setSQL(query).loadResult();
    }
}