<style>
#main { padding: 20px; }

#errors { color: red; }
</style>
<style>
.ace_editor {
    height: 500px;
    border: 1px solid #C0C0C0;
}
.ace_scrollbar { overflow: auto; }

button#save {
    margin: 10px 0;
}
span#save-icon {
    margin: 0 2px 1px 2px;
    padding-bottom: 2px;
}
span#save-text {
    margin-right: 3px;
    vertical-align: middle;
}
</style>

<div id="main">

    <p id="errors"></p>

    <form id="edit-config" name="edit-config" method="post">
        <input type="hidden" id="config" name="config" />
        <label for="title">Title</label><input type="text" id="title" name="title" />
        <div id="config-editor"></div>
        <button id="save" type="button">
            <span id="save-icon" class="dijitIcon saveIcon"></span><span id="save-text">Save</span>
        </button>
    </form>

</div>

<script type="text/javascript" src="/html/js/ace-builds-1.2.3/src-noconflict/ace.js" charset="utf-8"></script>
<script>
function showError(err) {
    console.error(err);
    document.getElementById("errors").innerHTML = err;
    document.getElementById("errors").scrollIntoView();
}

function clearErrors() {
    document.getElementById("errors").innerHTML = '';
}

var tuckeyapi = (function(){
    let domain = window.location.origin;

    function getCookie(name) {
        let re = RegExp('(?:(?:^|.*;\s*)'+name+'\s*\=\s*([^;]*).*$)|^.*$');
        return document.cookie.replace(re, "$1");
    }

    function authenticatedFetch(
        path = '',
        method = 'GET',
        body = null,
        thenFn = (response) => response.json(),
        errFn = (err) => showError(err),
        endpoint = '/api/tuckey/config',
    ){
        let headers = {
            "Authorization": "Bearer " + getCookie("access_token"),
        };
        if (method == 'POST') {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        return fetch(`${domain}${endpoint}${path}`, {
            body: body,
            headers: headers,
            method: method,
        })
        .then(thenFn)
        .catch(errFn);
    }

    return {
        list: () => authenticatedFetch(),
        save: (config, title) => {
            authenticatedFetch('/save', 'POST', `config=${encodeURIComponent(config)}&title=${encodeURIComponent(title)}`, (response) => {
                response.json().then((resWithBody) => {
                    if (response.ok) {
                        tuckeyapi.load(resWithBody.inode).then(response2 => {
                            let href = window.location.href;
                            let pIdIdx = href.indexOf("p_p_id");
                            let listUrl = href.slice(0, pIdIdx) + "p_p_id=TUCKEY_LIST";
                            window.location.replace(listUrl);
                        });
                    } else {
                        if (resWithBody.errors) {
                            showError(resWithBody.errors.join("\n"));
                        } else {
                            showError(`${response.status} ${response.statusText}`);
                        }
                    }
                });
            })
        },
        get: (inode) => authenticatedFetch(`/${inode}`),
        load: (inode) => authenticatedFetch(`/${inode}/load`, 'POST', null, null),
        archive: (inode) => authenticatedFetch(`/${inode}/archive`, 'POST', null, null),
        unarchive: (inode) => authenticatedFetch(`/${inode}/unarchive`, 'POST', null, null),
    };
}());
</script>
<script>
require(["dojo/dom", "dojo/dom-construct", "dojo/io-query", "dojo/domReady!"], function (dom, domConstruct, ioQuery) {

    // Wire up event handlers
    dom.byId("save").onclick = (event) => tuckeyapi.save(editor.getValue(), document.getElementById('title').value);

    // Instantiate editor
    var editor = ace.edit("config-editor");
    editor.setTheme("ace/theme/textmate");
    editor.getSession().setMode("ace/mode/xml");
    editor.getSession().setUseWrapMode(true);
    editor.clearSelection();

    // Are we editing or creating a configDoc?
    if (window.location.search && 'inode' in ioQuery.queryToObject(window.location.search.substring(1))) {
        // Edit
        let queryParams = ioQuery.queryToObject(window.location.search.substring(1));
        let inode = queryParams['inode'];
        tuckeyapi.get(inode).then(doc => {
            dom.byId("title").value = doc.title;
            editor.setValue(doc.contents);
        });
    } else {
        // Create
        tuckeyapi.list().then(docs => {
            let liveDoc = docs.find(doc => doc.live);
            editor.setValue(liveDoc.contents);
        });

    }
});
</script>