package edu.msu.anr.osgi.tuckey.api.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.dotcms.repackage.javax.ws.rs.*;
import com.dotcms.repackage.javax.ws.rs.core.CacheControl;
import com.dotcms.repackage.javax.ws.rs.core.Context;
import com.dotcms.repackage.javax.ws.rs.core.MediaType;
import com.dotcms.repackage.javax.ws.rs.core.Response;
import com.dotcms.repackage.javax.ws.rs.core.Response.ResponseBuilder;
import com.dotcms.rest.InitDataObject;
import com.dotcms.rest.WebResource;
import com.dotmarketing.business.APILocator;
import com.dotmarketing.business.DotStateException;
import com.dotmarketing.business.Role;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.util.Logger;
import com.liferay.portal.model.User;

import edu.msu.anr.osgi.tuckey.api.business.ConfigDocAPIImpl;
import edu.msu.anr.osgi.tuckey.api.model.ConfigDoc;
import edu.msu.anr.osgi.tuckey.api.model.ConfigDocValidationException;

/**
 * A Jersey REST API resource for {@link ConfigDoc}s.
 */
@Path("/tuckey/config")
public class ConfigDocResource {

    private static final String TUCKEY_USER_ROLE_NAME = "Tuckey User";

    private final WebResource webResource = new WebResource();

    /**
     * Lists stored {@link ConfigDoc}s.
     * @param request The {@link HttpServletRequest} object.
     * @return A {@link Response}. 200 with the body set to the list of {@link ConfigDoc}s if successful. 403 if user
     * is not authorized.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list(@Context HttpServletRequest request) {

        // Auth
        InitDataObject auth = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(auth.getUser())) {
                // Get configs
                List<ConfigDoc> docs = (new ConfigDocAPIImpl()).list();

                // Set response
                builder = Response.ok(docs);
            } else {
                builder = Response.status(403);
            }
        } catch (DotDataException e) {
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Attempts to save a submitted configuration document.
     * @param request The {@link HttpServletRequest} object.
     * @param config The XML configuration as a string.
     * @param title The title of the configuration as a string.
     * @return A {@link Response}. 201 with the "Location" header set to the new resource's URI if successfully created.
     * 400 if the given config was invalid. 403 if the user is not authorized. 500 if a DB error occurs.
     */
    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(@Context HttpServletRequest request, @FormParam("config") String config, @FormParam("title") String title) {

        // Get request data
        InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(initData.getUser())) {
                // Get submitted XML
                ConfigDoc savedDoc = (new ConfigDocAPIImpl()).save(config, title, initData.getUser());

                // Set response
                builder = Response.created(new URI("/api/tuckey/config/" + savedDoc.getInode())).entity(savedDoc);
            } else {
                builder = Response.status(403);
            }
        } catch (ConfigDocValidationException | URISyntaxException e) {
            // Invalid config
            Logger.error(this, e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(e);
        } catch (Exception e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Gets a stored ConfigDoc by its inode.
     * @param request The {@link HttpServletRequest} object.
     * @param inode The inode of the ConfigDoc to be returned.
     * @return A {@link Response}. 200 with the body set to the target {@link ConfigDoc} as JSON if found. 403 if the
     * user is not authorized. 404 if no {@link ConfigDoc} with the given inode is found. 500 if a DB error occurs.
     */
    @GET
    @Path("/{inode}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request, @PathParam("inode") String inode) {
        // Get request data
         InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
             if (isAuthorized(initData.getUser())) {
                // Get requested ConfigDoc
                ConfigDoc doc = (new ConfigDocAPIImpl()).get(inode);

                // Set response
                builder = Response.ok(doc);
             } else {
                 builder = Response.status(403);
             }
        } catch (IndexOutOfBoundsException e) {
            // Not found
            Logger.info(this, "Could not find ConfigDoc with inode "+inode+".");
            builder = Response.status(404);
        } catch (DotDataException e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Deletes a stored ConfigDoc by its inode.
     * @param request The {@link HttpServletRequest} object.
     * @param inode The inode of the ConfigDoc to be deleted.
     * @return A {@link Response}. 204 if successfully deleted. 403 if the user is not authorized. 404 if no
     * {@link ConfigDoc} with the given inode is found. 409 if the given {@link ConfigDoc} is currently the live
     * {@link ConfigDoc} and therefore cannot be deleted. 500 if a DB error occurs.
     */
    @DELETE
    @Path("/{inode}")
    public Response delete(@Context HttpServletRequest request, @PathParam("inode") String inode) {
        // Get request data
        InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(initData.getUser())) {
                // Delete requested ConfigDoc
                (new ConfigDocAPIImpl()).delete(inode);

                // Set response
                builder = Response.status(204);
            } else {
                builder = Response.status(403);
            }
        } catch (IndexOutOfBoundsException e) {
            // Not found
            Logger.info(this, "Could not find ConfigDoc with inode " + inode + ".");
            builder = Response.status(404);
        } catch (DotStateException e) {
            // Requested doc is currently the live doc, so we can't delete it
            Logger.info(this, "Unable to delete live ConfigDoc "+inode + ".");
            builder = Response.status(409);
        } catch (DotDataException|DotSecurityException e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Loads a stored ConfigDoc by its inode.
     * @param request The {@link HttpServletRequest} object.
     * @param inode The inode of the ConfigDoc to be loaded.
     * @return A {@link Response}. 200 if found. 403 if the user is not authorized. 404 if no {@link ConfigDoc} with
     * the given inode is found. 500 if a DB or UrlRewriteFilter error occurs.
     */
    @POST
    @Path("/{inode}/load")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response load(@Context HttpServletRequest request, @PathParam("inode") String inode) {
        // Get request data
        InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(initData.getUser())) {
                // Load requested ConfigDoc
                ConfigDoc doc = (new ConfigDocAPIImpl()).load(inode);

                // Set response
                builder = Response.ok();
            } else {
                builder = Response.status(403);
            }
        } catch (IndexOutOfBoundsException e) {
            // Not found
            Logger.info(this, "Could not find ConfigDoc with inode "+inode+".");
            builder = Response.status(404);
        } catch (Exception e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Archives a stored ConfigDoc by its inode.
     * @param request The {@link HttpServletRequest} object.
     * @param inode The inode of the ConfigDoc to be archived.
     * @return A {@link Response}. 200 if successfully archived. 403 if the user is not authorized. 404 if no
     * {@link ConfigDoc} with the given inode is found. 409 if the given {@link ConfigDoc} is live and therefore cannot
     * be archived. 500 if a DB error occurs.
     */
    @POST
    @Path("/{inode}/archive")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response archive(@Context HttpServletRequest request, @PathParam("inode") String inode) {
        // Get request data
        InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(initData.getUser())) {
                // Archive requested ConfigDoc
                ConfigDoc doc = (new ConfigDocAPIImpl()).archive(inode);

                // Set response
                builder = Response.ok();
            } else {
                builder = Response.status(403);
            }
        } catch (IndexOutOfBoundsException e) {
            // Not found
            Logger.info(this, "Could not find ConfigDoc with inode " + inode + ".");
            builder = Response.status(404);
        } catch (DotStateException e) {
            // Requested doc is currently the live doc, so we can't archive it
            Logger.info(this, "Unable to archive live ConfigDoc "+inode + ".");
            builder = Response.status(409);
        } catch (Exception e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Unarchives a stored ConfigDoc by its inode.
     * @param request The {@link HttpServletRequest} object.
     * @param inode The inode of the ConfigDoc to be unarchived.
     * @return A {@link Response}. 200 if successfully unarchived. 403 if the user is not authorized. 404 if no
     * {@link ConfigDoc} with the given inode is found. 500 if a DB error occurs.
     */
    @POST
    @Path("/{inode}/unarchive")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response unarchive(@Context HttpServletRequest request, @PathParam("inode") String inode) {
        // Get request data
        InitDataObject initData = webResource.init(true, request, true);

        // No cache
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);

        // Respond
        ResponseBuilder builder;
        try {
            if (isAuthorized(initData.getUser())) {
                // Unarchive requested ConfigDoc
                ConfigDoc doc = (new ConfigDocAPIImpl()).unarchive(inode);

                // Set response
                builder = Response.ok();
            } else {
                builder = Response.status(403);
            }
        } catch (IndexOutOfBoundsException e) {
            // Not found
            Logger.info(this, "Could not find ConfigDoc with inode "+inode+".");
            builder = Response.status(404);
        } catch (Exception e) {
            // DB error
            Logger.error(this, e.getMessage());
            builder = Response.serverError();
        }

        return builder.cacheControl(cc).build();
    }

    /**
     * Checks whether the given user is authorized to use this API.
     * @param user The user whose authorization is to be checked.
     * @return Whether the user is authorized to use this API. Authorization is determined by whether the user has a
     * role with name "Tuckey User".
     * @throws DotDataException If the system is unable to determine whether the given user has the role "Tuckey User".
     */
    private boolean isAuthorized(User user) throws DotDataException {
        Role tuckeyUserRole = APILocator.getRoleAPI().findRoleByFQN(TUCKEY_USER_ROLE_NAME);
        return APILocator.getRoleAPI().doesUserHaveRole(user, tuckeyUserRole);
    }
}
