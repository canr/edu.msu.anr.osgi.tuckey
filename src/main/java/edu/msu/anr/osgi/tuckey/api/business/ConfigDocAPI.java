package edu.msu.anr.osgi.tuckey.api.business;

import java.io.IOException;
import java.util.List;

import com.dotmarketing.business.DotStateException;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.liferay.portal.model.User;
import edu.msu.anr.osgi.tuckey.api.model.ConfigDoc;

/**
 * Provides access to {@link ConfigDoc} objects.
 */
public interface ConfigDocAPI {

    /**
     * Gets a list of all stored {@link ConfigDoc} objects.
     * @return List of all stored {@link ConfigDoc} objects.
     */
    List<ConfigDoc> list() throws DotDataException;

    /**
     * Attempts to save the given configuration document.
     * @param xml The configuration document as raw XML.
     * @param user The User who is creating this configuration document.
     * @throws DotDataException If the configuration cannot be saved to the database.
     * @throws DotSecurityException If the configuration cannot be saved to the database.
     * @throws Exception If the configuration is invalid.
     */
    ConfigDoc save(String xml, User user) throws Exception;

    /**
     * Attempts to save the given configuration document.
     * @param xml The configuration document as raw XML.
     * @param title The title of the configuration document to be saved.
     * @param user The User who is creating this configuration document.
     * @throws DotDataException If the configuration cannot be saved to the database.
     * @throws DotSecurityException If the configuration cannot be saved to the database.
     * @throws Exception If the configuration is invalid.
     */
    ConfigDoc save(String xml, String title, User user) throws Exception;

    /**
     * Attempts to save the given configuration document.
     * @param doc The configuration document to be saved.
     * @throws DotDataException If the configuration cannot be saved to the database.
     * @throws DotSecurityException If the configuration cannot be saved to the database.
     * @throws Exception If the configuration is invalid.
     */
    ConfigDoc save(ConfigDoc doc) throws Exception;

    /**
     * Gets a {@link ConfigDoc} by inode.
     * @param inode The inode of the {@link ConfigDoc} to be retrieved.
     * @return The target {@link ConfigDoc}.
     */
    ConfigDoc get(String inode) throws DotDataException;

    /**
     * Gets the live {@link ConfigDoc}.
     * @return The {@link ConfigDoc} currently marked as live.
     * @throws DotDataException If the live {@link ConfigDoc} cannot be retrieved.
     */
    ConfigDoc getLive() throws DotDataException;

    /**
     * Loads a {@link ConfigDoc} into the Tuckey rewrite engine.
     * @param inode The inode of the {@link ConfigDoc} to be loaded.
     * @return The loaded {@link ConfigDoc}.
     * @throws Exception If the DotUrlRewriteFilter cannot be reloaded.
     */
    ConfigDoc load(String inode) throws Exception;

    /**
     * Loads a {@link ConfigDoc} into the Tuckey rewrite engine.
     * @param doc The {@link ConfigDoc} to be loaded.
     * @return The loaded {@link ConfigDoc}.
     * @throws Exception If the DotUrlRewriteFilter cannot be reloaded.
     */
    ConfigDoc load(ConfigDoc doc) throws Exception;

    /**
     * Archives a {@link ConfigDoc}.
     * @param inode The inode of the {@link ConfigDoc} to be archived.
     * @return The archived {@link ConfigDoc}.
     * @throws DotStateException If the {@link ConfigDoc} is live.
     * @throws DotDataException If the {@link ConfigDoc} can't be set to archived in the database.
     * @throws DotSecurityException If the application cannot determine whether the {@link ConfigDoc} is live.
     */
    ConfigDoc archive(String inode) throws DotStateException, DotDataException, DotSecurityException;

    /**
     * Archives a {@link ConfigDoc}.
     * @param doc The {@link ConfigDoc} to be archived.
     * @throws DotStateException If the {@link ConfigDoc} is live.
     * @throws DotDataException If the {@link ConfigDoc} can't be set to archived in the database.
     * @throws DotSecurityException If the application cannot determine whether the {@link ConfigDoc} is live.
     */
    ConfigDoc archive(ConfigDoc doc) throws DotStateException, DotDataException, DotSecurityException;

    /**
     * Unarchives a {@link ConfigDoc}.
     * @param inode The inode of the {@link ConfigDoc} to be unarchived.
     * @throws DotDataException If the {@link ConfigDoc} can't be set to unarchived in the database.
     * @throws DotSecurityException If the application cannot determine whether the {@link ConfigDoc} is live.
     */
    ConfigDoc unarchive(String inode) throws DotDataException, DotSecurityException;

    /**
     * Unarchives a {@link ConfigDoc}.
     * @param doc The {@link ConfigDoc} to be unarchived.
     * @throws DotDataException If the {@link ConfigDoc} can't be set to unarchived in the database.
     * @throws DotSecurityException If the application cannot determine whether the {@link ConfigDoc} is live.
     */
    ConfigDoc unarchive(ConfigDoc doc) throws DotDataException, DotSecurityException;

    /**
     * Deletes a {@link ConfigDoc}.
     * @param inode The inode of the {@link ConfigDoc} to be deleted.
     * @throws DotDataException If the {@link ConfigDoc} can't be deleted from the database.
     * @throws DotStateException If the {@link ConfigDoc} is live.
     * @throws DotSecurityException If the live {@link ConfigDoc} can't be determined.
     */
    void delete(String inode) throws DotDataException, DotStateException, DotSecurityException;

    /**
     * Deletes a {@link ConfigDoc}.
     * @param doc The {@link ConfigDoc} to be deleted.
     * @throws DotDataException If the {@link ConfigDoc} can't be deleted from the database.
     * @throws DotStateException If the {@link ConfigDoc} is live.
     * @throws DotSecurityException If the live {@link ConfigDoc} can't be determined.
     */
    void delete(ConfigDoc doc) throws DotDataException, DotStateException, DotSecurityException;

    /**
     * Reads the current configuration from disk.
     * @return The current configuration.
     * @throws IOException If there is an error reading the file.
     */
    ConfigDoc readConfigFile() throws IOException;
}
