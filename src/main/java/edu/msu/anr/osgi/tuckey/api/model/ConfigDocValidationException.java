package edu.msu.anr.osgi.tuckey.api.model;

import java.util.List;

/**
 * Exception class which encapsulates errors validating a {@link ConfigDoc}.
 */
public class ConfigDocValidationException extends Exception {
    private final List<String> errors;

    public ConfigDocValidationException(String message, List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
