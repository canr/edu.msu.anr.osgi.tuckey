require(["dojo/dom", "dojo/dom-construct", "dojo/io-query", "dojo/domReady!"], function (dom, domConstruct, ioQuery) {

    // Wire up event handlers
    dom.byId("save").onclick = (event) => tuckeyapi.save(editor.getValue(), document.getElementById('title').value);

    // Instantiate editor
    var editor = ace.edit("config-editor");
    editor.setTheme("ace/theme/textmate");
    editor.getSession().setMode("ace/mode/xml");
    editor.getSession().setUseWrapMode(true);
    editor.clearSelection();

    // Are we editing or creating a configDoc?
    if (window.location.search && 'inode' in ioQuery.queryToObject(window.location.search.substring(1))) {
        // Edit
        let queryParams = ioQuery.queryToObject(window.location.search.substring(1));
        let inode = queryParams['inode'];
        tuckeyapi.get(inode).then(doc => {
            dom.byId("title").value = doc.title;
            editor.setValue(doc.contents);
        });
    } else {
        // Create
        tuckeyapi.list().then(docs => {
            let liveDoc = docs.find(doc => doc.live);
            editor.setValue(liveDoc.contents);
        });

    }
});
