function showError(err) {
    console.error(err);
    document.getElementById("errors").innerHTML = err;
    document.getElementById("errors").scrollIntoView();
}

function clearErrors() {
    document.getElementById("errors").innerHTML = '';
}

var tuckeyapi = (function(){
    let domain = window.location.origin;

    function getCookie(name) {
        let re = RegExp('(?:(?:^|.*;\s*)'+name+'\s*\=\s*([^;]*).*$)|^.*$');
        return document.cookie.replace(re, "$1");
    }

    function authenticatedFetch(
        path = '',
        method = 'GET',
        body = null,
        thenFn = (response) => response.json(),
        errFn = (err) => showError(err),
        endpoint = '/api/tuckey/config',
    ){
        let headers = {
            "Authorization": "Bearer " + getCookie("access_token"),
        };
        if (method == 'POST') {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        return fetch(`${domain}${endpoint}${path}`, {
            body: body,
            headers: headers,
            method: method,
        })
        .then(thenFn)
        .catch(errFn);
    }

    return {
        list: () => authenticatedFetch(),
        save: (config, title) => {
            authenticatedFetch('/save', 'POST', `config=${encodeURIComponent(config)}&title=${encodeURIComponent(title)}`, (response) => {
                response.json().then((resWithBody) => {
                    if (response.ok) {
                        tuckeyapi.load(resWithBody.inode).then(response2 => {
                            let href = window.location.href;
                            let pIdIdx = href.indexOf("p_p_id");
                            let listUrl = href.slice(0, pIdIdx) + "p_p_id=TUCKEY_LIST";
                            window.location.replace(listUrl);
                        });
                    } else {
                        if (resWithBody.errors) {
                            showError(resWithBody.errors.join("\n"));
                        } else {
                            showError(`${response.status} ${response.statusText}`);
                        }
                    }
                });
            });
        },
        get: (inode) => authenticatedFetch(`/${inode}`),
        load: (inode) => authenticatedFetch(`/${inode}/load`, 'POST', null, null),
        archive: (inode) => authenticatedFetch(`/${inode}/archive`, 'POST', null, null),
        unarchive: (inode) => authenticatedFetch(`/${inode}/unarchive`, 'POST', null, null),
    };
}());