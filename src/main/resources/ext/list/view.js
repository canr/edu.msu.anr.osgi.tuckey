require(["dojo/dom", "dojo/dom-construct", "dojo/query", "dojo/domReady!"], function (dom, domConstruct, query) {
    /**
     * Table Rendering
     */

    function configDocsToHtml(docs) {
        return docs.reduce((acc, doc) => {
            return acc + configDocToHtml(doc);
        }, "");
    }

    function configDocToHtml(doc) {
        let date = new Date(doc.modDate);
        return `
        <tr class="config-doc">
            <td class="inode"><a href="../edit/view.html?inode=${doc.inode}">${doc.inode}</a></td>
            <td class="title">${doc.title}</td>
            <td class="mod-date">${date.toLocaleString()}</td>
            <td class="mod-user">${doc.modUser}</td>
            <td class="archived">${doc.archived}</td>
            <td class="live">${doc.live}</td>
            <td>
                <button class="load">Load</button>
                <button class="archive">${doc.archived ? "Unarchive" : "Archive"}</button>
            </td>
        </tr>`;
    }

    function updateCurrentConfigDocs() {
        let current_configs = dom.byId("current-configs-body");
        tuckeyapi.list().then(docs => {
            // Update table HTML
            current_configs.innerHTML = configDocsToHtml(
                docs.sort((a, b) => {
                    if (a.live != b.live) { // Sort by which node is live
                        return a.live < b.live ? 1 : -1
                    } else if (a.modDate != b.modDate) { // Sort by mod date desc
                        return a.modDate < b.modDate ? 1 : -1
                    } else {
                        return 0 // They're the same
                    }
                })
            );

            // Wire button event handlers
            query(".load").forEach(node => node.onclick = onLoadButtonClick);
            query(".archive").forEach(node => node.onclick = onArchiveButtonClick);
        });
    }


    /**
     * Action Button Handling
     */

    function getTargetInode(event) {
        return query("td.inode a", getTargetConfigDoc(event))[0].innerHTML;
    }

    function getTargetConfigDoc(event) {
        return event.target.parentElement.parentElement;
    }

    function onActionButtonClick(event, fetchFn) {
        let inode = getTargetInode(event);
        console.log(`${fetchFn.name}: ${inode}`);
        clearErrors();
        fetchFn.call(event, inode)
            .then(doc => updateCurrentConfigDocs());
    }

    function onLoadButtonClick(event) { onActionButtonClick(event, tuckeyapi.load); }

    function onArchiveButtonClick(event) {
        let archived = query("td.archived", getTargetConfigDoc(event))[0].innerHTML;
        let actionFn = (archived == "true") ? tuckeyapi.unarchive : tuckeyapi.archive;
        onActionButtonClick(event, actionFn);
    }



    /**
     * Entrypoint
     */

    // Populate table
    updateCurrentConfigDocs();
});
