This plugin provides a REST API to manage Tuckey configurations and a portlet which provides a GUI to that REST API.

# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.


## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework you should upload the compiled plugin .jar through dotCMS' Dynamic Plugins portlet. See [dotCMS' documentation on the Dynamic Plugins portlet](https://dotcms.com/docs/latest/osgi-plugins#Portlet).

# Configuration

## Naming the Portlets
Each portlet's name in the backend is a configurable Language property. In order to set a portlet's name in the backend do the following:

1. Log in to dotCMS.
2. Open the Languages portlet (Types & Tags > Languages).
3. Select a language. For example, click "English (US)".
4. Click the "Add New Property" button.
5. In the new Property's "Language Key" field type the portlet's language key. The language key should be in the format "com.dotcms.repackage.javax.portlet.title.${portletName}". For example, to name a portlet whose short name is "TUCKEY_LIST" we must create the Language Key "com.dotcms.repackage.javax.portlet.title.TUCKEY_LIST".
6. In either of the other two of the new Property's fields type the portlet's desired name.
    * The first of the other two fields is a language-wide value. For example, if our UI is in English and we set this field to "Tuckey GUI", all English speakers will see "Tuckey GUI".
    * The second of the other two fields is a country-specific value. For example, if our UI is in English and our country is set to "US" and we set this field to "Tuckey GUI for the USA", English speakers in the US will see "Tuckey GUI for the USA".
7. Click the "Save Changes" button.


## Adding the Portlet(s) to the Main Menu

In order to make this plugin's portlets / Tools visible to users they must be added to a Tool Group in the main menu. That Tool Group must be configured to be available to target users. For more detailed information see [dotCMS' documentation on Roles and Tools](https://dotcms.com/docs/latest/roles-and-tools).

1. Log in to dotCMS.
2. Open the Roles & Tools portlet (System > Roles & Tools).
3. Select or create a Role whose users should have access to this portlet/ Tool.
4. Add one of this plugin's tools portlet/ Tool to a Tool Group:
    1. In the right-hand pane, open the "Tools" tab.
    2. Click on either an existing Tool Group or the "Create Custom Tool Group" button.
    3. In the "Tools" dropdown field, select this portlet by its language name. See the section [Naming the Portlets](#Naming%20the%20Portlets) for information on setting its language name.
    4. Click the "Save" button.
5. Make that Tool Group available to the target Role:
    1. Check the checkbox next to the target Tool Group.
    2. Click the "Save" button.
6. Assign the target Role to your target Users:
    1. In the righ-hand pane, open the "Users" tab.
    2. For each target User:
        1. In the "Grant" dropdown, select your target User.
        2. Click the "Grant" button.


## Granting User Access

Users must have the role "Tuckey User" in order to access the REST API resource. If a user does not have the role "Tuckey User" they must have it granted to them. If the role "Tuckey User" does not exist it must be created.

# GUI Usage

## Listing Configurations

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.

## Creating a New Configuration Based on the Live Configuration

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.
2. Click the "New" link.
3. Edit your config and its title.
4. Click the "Save" button.

## Creating a New Configuration Based on a Selected Configuration

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.
2. Locate your target base configuration.
3. Click on the target base configuration's inode link.
4. Edit your config and its title.
5. Click the "Save" button.

## Loading an Existing Configuration

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.
2. Locate your target non-live configuration.
3. Click on the "Load" button next to your target configuration.

## Archiving an Existing Configuration

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.
2. Locate your target unarchived configuration.
3. Click on the "Archive" button next to your target configuration.

## Unarchiving an Existing Configuration

1. Open the TUCKEY_LIST portlet. This is probably located at (System > Tuckey GUI). If not, contact your CMS administrator to determine the tool's configured location.
2. Locate your target archived configuration.
3. Click on the "Unarchive" button next to your target configuration.

# REST API Endpoints

## ConfigDocResource

The class edu.msu.anr.osgi.tuckey.api.rest.ConfigDocResource is available at the path "/api/tuckey/config". All methods require the request to be authenticated using [JSON Web Tokens (JWT)](https://jwt.io/). To use JWT, a HTTP request must have the header "Authorization" set to the value "Bearer ${token}" where `${token}` is the JWT token string. The JWT token string is given to the client as a cookie when the user logs in to dotCMS, and can be read from the cookie "access_token".

REST methods available on ConfigDocResource are as follows:

| Endpoint  | HTTP Method | Description                                                                                                                                                                                                                                                                 | Path                                 | Body                                                                                                                                                                                                                                              | Login Required |
|-----------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| list      | GET         | Lists all currently saved ConfigDocs.                                                                                                                                                                                                                                       | /api/tuckey/config                   |                                                                                                                                                                                                                                                   | Yes            |
| save      | POST        | Attempts to save a ConfigDoc.                                                                                                                                                                                                                                               | /api/tuckey/config/save              | application/x-www-form-urlencoded with the following fields:  "config": The Tuckey XML configuration document as a string. See: http://www.tuckey.org/urlrewrite/manual/2.6/  "title": The title of the given configuration document as a string. | Yes            |
| get       | GET         | Gets a stored ConfigDoc by its inode.                                                                                                                                                                                                                                       | /api/tuckey/config/{inode}           |                                                                                                                                                                                                                                                   | Yes            |
| delete    | DELETE      | Permanently deletes a ConfigDoc by its inode.                                                                                                                                                                                                                               | /api/tuckey/config/{inode}/delete    |                                                                                                                                                                                                                                                   | Yes            |
| load      | POST        | Loads a ConfigDoc by its inode. This makes the ConfigDoc's configuration the active Tuckey configuration used by dotCMS. Any old Tuckey rules from a previously loaded ConfigDoc, another plugin, or the system's initial urlrewrite.xml file will be removed and replaced. | /api/tuckey/config/{inode}/load      |                                                                                                                                                                                                                                                   | Yes            |
| archive   | POST        | Archives a ConfigDoc by its inode.                                                                                                                                                                                                                                          | /api/tuckey/config/{inode}/archive   |                                                                                                                                                                                                                                                   | Yes            |
| unarchive | POST        | Unarchives a ConfigDoc by its inode.                                                                                                                                                                                                                                        | /api/tuckey/config/{inode}/unarchive |                                                                                                                                                                                                                                                   | Yes            |