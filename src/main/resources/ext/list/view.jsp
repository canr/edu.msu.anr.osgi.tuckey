<style>
#main { padding: 20px; }

#errors { color: red; }
</style>

<div id="main">

    <p id="errors"></p>

    <a id="create-config-doc" class=".button">New</a>

    <table id="current-configs">
        <thead>
            <tr>
                <th>inode</th>
                <th>title</th>
                <th>modDate</th>
                <th>modUser</th>
                <th>archived</th>
                <th>live</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody id="current-configs-body">
        </tbody>
    </table>

</div>

<script type="text/javascript">
function showError(err) {
    console.error(err);
    document.getElementById("errors").innerHTML = err;
    document.getElementById("errors").scrollIntoView();
}

function clearErrors() {
    document.getElementById("errors").innerHTML = '';
}

var tuckeyapi = (function(){
    let domain = window.location.origin;

    function getCookie(name) {
        let re = RegExp('(?:(?:^|.*;\s*)'+name+'\s*\=\s*([^;]*).*$)|^.*$');
        return document.cookie.replace(re, "$1");
    }

    function authenticatedFetch(
        path = '',
        method = 'GET',
        body = null,
        thenFn = (response) => response.json(),
        errFn = (err) => showError(err),
        endpoint = '/api/tuckey/config',
    ){
        let headers = {
            "Authorization": "Bearer " + getCookie("access_token"),
        };
        if (method == 'POST') {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        return fetch(`${domain}${endpoint}${path}`, {
            body: body,
            headers: headers,
            method: method,
        })
        .then(thenFn)
        .catch(errFn);
    }

    return {
        list: () => authenticatedFetch(),
        save: (config, title) => {
            authenticatedFetch('/save', 'POST', `config=${encodeURIComponent(config)}&title=${encodeURIComponent(title)}`, (response) => {
                response.json().then((resWithBody) => {
                    if (response.ok) {
                        tuckeyapi.load(resWithBody.inode).then(response2 => {
                            let href = window.location.href;
                            let pIdIdx = href.indexOf("p_p_id");
                            let listUrl = href.slice(0, pIdIdx) + "p_p_id=TUCKEY_LIST";
                            window.location.replace(listUrl);
                        });
                    } else {
                        if (resWithBody.errors) {
                            showError(resWithBody.errors.join("\n"));
                        } else {
                            showError(`${response.status} ${response.statusText}`);
                        }
                    }
                });
            });
        },
        get: (inode) => authenticatedFetch(`/${inode}`),
        load: (inode) => authenticatedFetch(`/${inode}/load`, 'POST', null, null),
        archive: (inode) => authenticatedFetch(`/${inode}/archive`, 'POST', null, null),
        unarchive: (inode) => authenticatedFetch(`/${inode}/unarchive`, 'POST', null, null),
    };
}());
</script>
<script type="text/javascript">
require(["dojo/dom", "dojo/dom-construct", "dojo/query", "dojo/domReady!"], function (dom, domConstruct, query) {
    // Inter-portlet URLs
    let href = window.location.href;
    let pIdIdx = href.indexOf("p_p_id");
    let editUrl = href.slice(0, pIdIdx) + "p_p_id=TUCKEY_EDIT";
    window.document.getElementById('create-config-doc').href = editUrl;

    /**
     * Table Rendering
     */

    function configDocsToHtml(docs) {
        return docs.reduce((acc, doc) => {
            return acc + configDocToHtml(doc);
        }, "");
    }

    function configDocToHtml(doc) {
        let date = new Date(doc.modDate);
        return `
        <tr class="config-doc">
            <td class="inode"><a href="${editUrl}&inode=${doc.inode}">${doc.inode}</a></td>
            <td class="title">${doc.title}</td>
            <td class="mod-date">${date.toLocaleString()}</td>
            <td class="mod-user">${doc.modUser}</td>
            <td class="archived">${doc.archived}</td>
            <td class="live">${doc.live}</td>
            <td>
                <button class="load">Load</button>
                <button class="archive">${doc.archived ? "Unarchive" : "Archive"}</button>
            </td>
        </tr>`;
    }

    function updateCurrentConfigDocs() {
        let current_configs = dom.byId("current-configs-body");
        tuckeyapi.list().then(docs => {
            // Update table HTML
            current_configs.innerHTML = configDocsToHtml(
                docs.sort((a, b) => {
                    if (a.live != b.live) { // Sort by which node is live
                        return a.live < b.live ? 1 : -1
                    } else if (a.modDate != b.modDate) { // Sort by mod date desc
                        return a.modDate < b.modDate ? 1 : -1
                    } else {
                        return 0 // They're the same
                    }
                })
            );

            // Wire button event handlers
            query(".load").forEach(node => node.onclick = onLoadButtonClick);
            query(".archive").forEach(node => node.onclick = onArchiveButtonClick);
        });
    }


    /**
     * Action Button Handling
     */

    function getTargetInode(event) {
        return query("td.inode a", getTargetConfigDoc(event))[0].innerHTML;
    }

    function getTargetConfigDoc(event) {
        return event.target.parentElement.parentElement;
    }

    function onActionButtonClick(event, fetchFn) {
        let inode = getTargetInode(event);
        console.log(`${fetchFn.name}: ${inode}`);
        clearErrors();
        fetchFn.call(event, inode)
            .then(doc => updateCurrentConfigDocs());
    }

    function onLoadButtonClick(event) { onActionButtonClick(event, tuckeyapi.load); }

    function onArchiveButtonClick(event) {
        let archived = query("td.archived", getTargetConfigDoc(event))[0].innerHTML;
        let actionFn = (archived == "true") ? tuckeyapi.unarchive : tuckeyapi.archive;
        onActionButtonClick(event, actionFn);
    }



    /**
     * Entrypoint
     */

    // Populate table
    updateCurrentConfigDocs();
});
</script>
